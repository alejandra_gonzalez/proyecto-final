function cargaMapa() {
    let mapa = L.map('divMapa', { center: [4.641291, -74.162406], zoom: 16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);


    let markerBiblioteca= L.marker([4.643055556, -74.15444444]);
    markerBiblioteca.addTo(mapa).bindPopup("<h1>Biblioteca el tintal</h1><br>La Biblioteca el Tintal abrió sus puertas hace 19 años.Esta ubicada sobre la Avenida Ciudad de Cali.Antiguamente funcionaba como una planta de tratamiento de basuras Protecho de la empresa EDIS. Esta hace parte de las 20 bibliotecas públicas de Bogotá.")
    .openPopup();

    let markerTintal= L.marker([4.642482604393017, -74.1564354300499]);
    markerTintal.addTo(mapa).bindPopup("<h1>Centro Comercial Tintal Plaza</h1><br> Este lugar se encuentra ubicado sobre la Avenida américas con Avenida ciudad de Cali. Cuenta con una variedad de tiendas que ofrecen gran variedad de servicios. Allí se pueden encontrar franquicias como el éxito, smathFit, Droguerías Colsubsidio, entre otras.")
    .openPopup();

    let markerEstacion= L.marker([4.639264, -74.160578]);
    markerEstacion.addTo(mapa).bindPopup("<h1>Estación de transmilenio Biblioteca Tintal </h1><br> La estación está ubicada en el sector suroccidental de la ciudad, más específicamente sobre la Avenida Ciudad de Cali entre calles 2 y 34 Sur. Se accede a ella a través de cruces semaforizados ubicados sobre estas vías.Atiende la demanda de los barrios Patio Bonito, María Paz y sus alrededores.").openPopup();

    
    let markerParque= L.marker([4.640984, -74.164343]);
    markerParque.addTo(mapa).bindPopup("<h1>Parques Distritaes Patio Bonito</h1><br> Este es un centro recreativo y deportivo. Cuenta con un parque y una piscina cubierta y climatizada.Su dirección es calle 34Bis sur No. 88D-12.").openPopup();

    let markerCade= L.marker([4.641828, -74.157897]);
    markerCade.addTo(mapa).bindPopup("<h1>CADE Patio Bonito</h1><br> Presta un servicio integral a la ciudadanía, en información, asesoría y trámites. Aquí los habitantes del barrio pueden acercarse y pagar sus servicios públicos. ").openPopup();

    let markerColsubsidio= L.marker([4.649211, -74.167847]);
    markerColsubsidio.addTo(mapa).bindPopup("<h1>Supermercado y Droguería Colsubsidio</h1><br>Se encuentra ubucado en la dirección D 06, Cl. 26 Sur #3393. Este lugar ofrece gran variedad de productos para la canasta familiar y además cuenta con droguería para adquirir medicamentos. ").openPopup();

    
}
