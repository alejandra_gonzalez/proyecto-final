function cargaMapa() {
    let mapa = L.map('divMapa', { center: [4.641291, -74.162406], zoom: 16 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

    let markerBiblioteca= L.marker([4.643055556, -74.15444444]);
    markerBiblioteca.addTo(mapa);

    let markerTintal= L.marker([4.642482604393017, -74.1564354300499]);
    markerTintal.addTo(mapa);

    let markerComunal= L.marker([4.6420684, -74.159475]);
    markerComunal.addTo(mapa);

    
    let markerParque= L.marker([4.640984, -74.164343]);
    markerParque.addTo(mapa);

}
